<?php 
	/**
	 * 
	 */
	class ServiceModel {
		
		public $id_servicio = '';
		public $ser_nombre = '';
		public $ser_id_tipo_servicio = '';
		public $ser_tipo_nombre = '';
		public $ser_description = '';
		public $ser_precio = '';
		public $ser_fecha_creación = '';
		public $ser_fecha_actualizacion= '';
		public $ser_iva = '';
		public $ser_status = '';
 
		
		public function getIdServicio(){
			return $this->id_servicio;
		}

		public function setIdServicio($id_servicio){
			$this->id_servicio = $id_servicio;
		}

		public function getSerNombre(){
			return $this->ser_nombre;
		}

		public function setSerNombre($ser_nombre){
			$this->ser_nombre = $ser_nombre;
		}

		public function getTipoNombre(){
			return $this->ser_tipo_nombre;
		}

		public function setTipoNombre($ser_tipo_nombre){
			$this->ser_tipo_nombre = $ser_tipo_nombre;
		}

		public function getIdTipoServicio(){
			return $this->ser_id_tipo_servicio;
		}

		public function setIdTipoServicio($ser_id_tipo_servicio){
			$this->ser_id_tipo_servicio = $ser_id_tipo_servicio;
		}

		public function getSerDescription(){
			return $this->ser_description;
		}

		public function setSerDescription($ser_description){
			$this->ser_description = $ser_description;
		}

		public function getSerPrecio(){
			return $this->ser_precio;
		}

		public function setSerPrecio($ser_precio){
			$this->ser_precio = $ser_precio;
		}

		public function getSerFechaCreacion(){
			return $this -> $ser_fecha_creacion;
		}

		public function setSerFechaCreacion($ser_fecha_creacion){
			$this->ser_fecha_creacion = $ser_fecha_creacion;
		}

		public function getSerFechaActualizacion(){
			return $this->ser_fecha_actualizacion;
		}

		public function setSerFechaActualizacion($ser_fecha_actualizacion){
			$this->ser_fecha_actualizacion = $ser_fecha_actualizacion;
		}

		public function getSerIva(){
			return $this->ser_iva;
		}

		public function setSerIva($ser_iva){
			$this->ser_iva = $ser_iva;
		}

		public function getSerStatus(){
			return $this->ser_status;
		}

		public function setSerStatus($ser_status){
			$this->ser_status = $ser_status;
		}


	}
?>