<?php



if(isset($_POST['function']) && !empty($_POST['function'])) {

	$function = $_POST['function'];
    
    
	//En función del parámetro que nos llegue ejecutamos una función u otra
    switch($function) {

        case 'getServices': 
        	
        	require_once 'conn/connection.php';
	        
            $connect = new connection();
			$connection=$connect->connections();

			$sql = "SELECT * FROM servicios s, tipo_servicio ts WHERE ser_status = 1 and s.id_tipo_servicio = ts.id_tipo_servicio ORDER BY id_servicio";
			$result = mysqli_query($connection, $sql);

			$tabla = "";
			
			while($row = mysqli_fetch_array($result)){

				$id = $row['id_servicio'];
				$id_tipo_servicio = $row['id_tipo_servicio'];
				$nombre = $row['ser_nombre'];
				$tipo = $row['ts_nombre'];
				$descripcion = $row['ser_descripcion'];
				$precio = $row['ser_precio'];
				$img_url = $row['ser_img_url'];

				$eliminar = '<a onclick=\"showConfirm(this)\" data-toggle=\"tooltip\" data-id=\"'.$id.'\" data-placement=\"top\" title=\"Editar\" class=\"btn btn-danger btn-circle\"><i class=\"fa fa-trash\" style=\"color:white !important;\" aria-hidden=\"true\"></i></a>';

				$editar = '<a  onclick=\"showDetails(this)\" data-toggle=\"tooltip\" style=\"color:white !important;\" data-placement=\"top\" data-id=\"'.$id.'\" data-url=\"'.$img_url.'\" data-name=\"'.$nombre.'\" data-desc=\"'.$descripcion.'\" data-cost=\"'.$precio.'\" data-id-tipo=\"'.$id_tipo_servicio.'\" title=\"Eliminar\" class=\"btn btn-warning btn-circle\"><i class=\"fa fa-pencil-alt\" aria-hidden=\"true\"></i></a>';
				
					
				$tabla.='{
					"id":"'.$id.'",
					"nombre":"'.$nombre.'",
					"tipo":"'.$tipo.'",
					"descripcion":"'.$descripcion.'",
					"precio":"'.$precio.'",
					"acciones":"'.$editar. " " .$eliminar.'"
				},';	

			}	
			//eliminamos la coma que sobra
			$tabla = substr($tabla,0, strlen($tabla) - 1);
			echo '{"data":['.$tabla.']}';	

            break;

        case 'addService':
        	# code...
        	$name = $_POST['name'];
        	$cost = $_POST['cost'];
        	$desc = $_POST['desc'];
        	$id_tipo_servicio = $_POST['id_tipo_servicio'];
        	$date = date("Y-m-d");
        	$iva = 0;
        	$status = 1;
  			
        	$nombre = $_FILES['archivo']['name'];
			//$ruta = $_FILES['archivo']['tmp_name'];
			$destino = "../img/img-services/" . $nombre;

			if (move_uploaded_file($_FILES["archivo"]["tmp_name"], $destino)) {
		       $message= "El Archivo ha sido subido correctamente.";
			   
			   
		    } else {
		       $message= "Lo sentimos, hubo un error subiendo el archivo.";
		    }

        	require_once 'conn/connection.php';
        	$connect = new connection();
			$connection=$connect->connections();
			
			
			$sql = "INSERT INTO servicios (id_tipo_servicio, ser_nombre, ser_descripcion, ser_img_url, ser_precio, ser_fecha_creacion, ser_fecha_actualizacion, ser_iva, ser_status) VALUES ('".$id_tipo_servicio ."','".$name."','".$desc."','".$destino."','".$cost."','".$date ."', '".$date."', '".$iva."', '".$status."');";
			

			$jsondata = array();


        	if ($connection->query($sql)===true) {
			    $jsondata['success'] = true;
        		$jsondata['message'] = $message;

			} else {
			    $jsondata['success'] = false;
        		$jsondata['message'] = $message;
			}
			//Aunque el content-type no sea un problema en la mayoría de casos, es recomendable especificarlo
		    header('Content-type: application/json; charset=utf-8');
		    
		    echo json_encode($jsondata, JSON_FORCE_OBJECT);


        	break;

        case 'edithService':
        	# code...
        	require_once 'conn/connection.php';
			
			$id_product = $_POST['id'];
			$name = $_POST['name'];
        	$desc = $_POST['desc'];
        	$cost = $_POST['cost'];
        	$id_tipo_servicio = $_POST['id_tipo_servicio'];
        	$date = date("Y-m-d");

        	$connect = new connection();
			$connection=$connect->connections();
			
			if(empty($_FILES['archivo']['name'])) {
			 //Seleccione un archivo
				$nombre = $_POST['archivo'];

				$sql = "UPDATE servicios set id_tipo_servicio = '" . $id_tipo_servicio . "', ser_nombre='" . $name . "', ser_descripcion='" . $desc . "', ser_precio='" . $cost . "', ser_fecha_actualizacion='" . $date . "'  WHERE id_servicio='" . $id_product . "'";

			}else{


				$nombre = $_FILES['archivo']['name'];

				$destino = "../img/img-services/" . $nombre;

				$sql = "UPDATE servicios set id_tipo_servicio = '" . $id_tipo_servicio . "', ser_img_url='" . $destino . "', ser_nombre='" . $name . "', ser_descripcion='" . $desc . "', ser_precio='" . $cost . "', ser_fecha_actualizacion='" . $date . "'  WHERE id_servicio='" . $id_product . "'";


				if (move_uploaded_file($_FILES["archivo"]["tmp_name"], $destino)) {
			       $message= "El Archivo ha sido subido correctamente.";
				   
				   
			    } else {
			       $message= "Lo sentimos, hubo un error subiendo el archivo.";
			    }
			}


        	
        	$jsondata = array();

        	if ($connection->query($sql)===true) {
			    $jsondata['success'] = true;
        		$jsondata['message'] = 'Felicidades! Has eliminado este servicio.';

			} else {
			    $jsondata['success'] = false;
        		$jsondata['message'] = 'Error! Ha ocurrido un error, avisa a un administrador.';
			}
			//Aunque el content-type no sea un problema en la mayoría de casos, es recomendable especificarlo
		    header('Content-type: application/json; charset=utf-8');
		    
		    echo json_encode($jsondata, JSON_FORCE_OBJECT);

        	break;

        case 'deleteService':
        	# code...
	        require_once 'conn/connection.php';
			
			$id_product = $_POST['id'];

			$connect = new connection();
			$connection=$connect->connections();
			$sql = "UPDATE servicios SET ser_status = 0 WHERE id_servicio = " . $id_product .";";
        	
        	$jsondata = array();

        	if ($connection->query($sql)===true) {
			    $jsondata['success'] = true;
        		$jsondata['message'] = 'Felicidades! Has eliminado este servicio.';

			} else {
			    $jsondata['success'] = false;
        		$jsondata['message'] = 'Error! Ha ocurrido un error, avisa a un administrador.';
			}
			//Aunque el content-type no sea un problema en la mayoría de casos, es recomendable especificarlo
		    header('Content-type: application/json; charset=utf-8');
		    
		    echo json_encode($jsondata, JSON_FORCE_OBJECT);

			break;
    }


}
		
?>