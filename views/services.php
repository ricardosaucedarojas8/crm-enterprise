<?php
include '../controller/conn/connection.php';
          
  $connect = new connection();
  $connection=$connect->connections();

  $sql = "SELECT * FROM tipo_servicio WHERE ts_status = 1 ORDER BY id_tipo_servicio";
  $result = mysqli_query($connection, $sql);

  include 'header.php';
?>
  
  <script src="../assets/js/services.js"></script>
    <title>Services</title>
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <?php include 'side-bar.php';?>

    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <?php include 'nav-wrapper.php';?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
                  <!-- Begin Page Content -->
        <div class="container-fluid">
        <div class="row">
            <main role="main" class="col-md-12 col-lg-12 pt-3 px-4">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                    <h1 class="h2"><i class="fas fa-boxes fa-lg"></i> Services</h1>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-4">
                        <!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
                        <a href="#" data-toggle="modal" data-target="#myModal" class="btn btn-success btn-block" onclick="resetModal()" >Add Service <i class="fas fa-plus"></i>
                        </a>
                    </div>
                    <div class="col-md-4">

                    </div>
                    <div class="col-md-4">

                    </div>
                </div>
                <br><br>

                <div class="table-responsive">
                    <table id="services" class="display nowrap" style="width:100%">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Tipo de Servicio</th>
                                <th>Descripción</th>
                                <th>Precio</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Tipo de Servicio</th>
                                <th>Descripción</th>
                                <th>Precio</th>
                                <th>Acciones</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <br><br>
                <h1 class="h2">Reports</h1>
                <br>
                <div class="row">
                    <div class="col-md-4">
                        <!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
                        <button type="button" class="btn btn-primary btn-block">Week</button>
                        <br>
                    </div>
                    <div class="col-md-4">
                        <!-- Contextual button for informational alert messages -->
                        <button type="button" class="btn btn-info btn-block">Month</button>
                        <br>

                    </div>
                    <div class="col-md-4">
                        <!-- Indicates a successful or positive action -->
                        <button type="button" class="btn btn-success btn-block">Year</button>
                        <br>
                    </div>
                </div>
                <br><br>
            </main>
        </div>
    </div>
        <!-- /.container-fluid -->

  <!-- The ADD and EDITH Modal -->
  <div class="modal fade" id="myModal" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title"><p id="modalTitle"></p></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <form id="myForm" enctype="multipart/form-data" method="post">
        <!-- Modal body -->
        <br>
        <img src="" id="img-gral" name="img-gral" class="rounded mx-auto none-display-custom d-block" alt="Cinque Terre" width="100" height="100"> 
        <br>
        <div class="upload-btn-wrapper">
          <button class="upload-btn">Upload a file</button>
          <input type="file" name="img_url" id="img_url" />
        </div>
        <div class="modal-body">

          <input type="text" id="id" name="id" value="" hidden required="">
          <input type="text" id="url_edith" name="url_edith" value="" hidden required="">
          <label>Service Name</label>
          <input type="text" id="name" name="name" value=""  onkeypress="return soloLetras(event)" onblur="limpia()" class="form-control validate" required="" placeholder="Enter Service Name">
          <p id="errorName" style="color:red; display: none;"></p>
          <label>Service Type</label>
          <select class="form-control" id="id_tipo_servicio" name="id_tipo_servicio">
            <option value="0">Select option...</option>
            <?php
              while ($valores = mysqli_fetch_array($result)) {
                echo '<option value="'.$valores[id_tipo_servicio].'">'.$valores[ts_nombre].'</option>';
              }
            ?>
          </select>
          <p id="errorSelect" style="color:red; display: none;"></p>
          <label>Description</label>
          <textarea id="desc" name="desc" class="form-control validate" required="" placeholder="Enter Service Description"></textarea>
          <p id="errorDesc" style="color:red; display: none;"></p>
          <label>Cost</label>
          <input type="number" id="cost" name="cost" value="" class="form-control validate" required="" placeholder="Enter Cost">
          <p id="errorCost" style="color:red; display: none;"></p>
          <div class="spinner-grow text-primary" style="display: none;" role="status" id="showLoad">
            <span class="sr-only">Loading...</span>
          </div>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-success" id="saveButton" onclick="doAction()" style="display: none"><span id="load" class="spinner-border spinner-border-sm" role="status" style="display: none" aria-hidden="true"></span> Save</button>

          <button type="button" class="btn btn-warning" id="edithButton" style="color:white;" onclick="doAction()" >Edith</button>

          <button type="button" class="btn btn-danger" data-dismiss="modal" >Cancel</button>
        </div>
        </form>
      </div>
    </div>
  </div>

   <!-- DELETE Modal -->
  <div class="modal fade" id="deleteModal" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Delete Product</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <form>
        <!-- Modal body -->
        <div class="modal-body">
          <input type="text" id="id" value="" hidden >
          <h3>Do you want delete this product??</h3>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-success" >Save</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
        </div>
        </form>
      </div>
    </div>
  </div>

  <!-- SUCCESS / FAIL ACTION MODAL -->
  
  <div class="modal fade" id="actionModal" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h2 class="modal-title"><p id="successModalTitle"></p></h2>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <form>
        <!-- Modal body -->
        <div class="modal-body">
          <p id="successModalDescription"></p>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-success" onclick="confirm()">Agree</button>
        </div>
        </form>
      </div>
    </div>
  </div>

  <!-- The CONFIRM ACTION Modal -->
  <div class="modal fade" id="confirmActionModal" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title"><p id="confirmTitle"></p></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <form>
        <!-- Modal body -->
        <div class="modal-body">
          <input type="text" id="id_confirm" value="" hidden >
          <p id="confirmDescription"></p>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" onclick="deleteService()">Delete</button>
          <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
        </div>
        </form>
      </div>
    </div>
  </div>
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <?php include 'copy.php';?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <script src="../assets/js/ajaxServices.js"></script>

  <?php include 'footer.php';?>

</body>

</html>
<?php

?>
