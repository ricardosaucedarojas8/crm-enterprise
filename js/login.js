function doAction(){
	var correo = $('#email-login')[0].value;
	var password = $('#pass-login')[0].value;
	

	var parametros = {
        "email-login" : correo,
        "pass-login" : password,
        "function" : "getAccess"
      };
      $.ajax({
        data:  parametros, //send data via AJAX
        url:   'controller/loginController.php', //url file controller PHP
        type:  'post', //send POST data
        beforeSend: function () {
          document.getElementById("load").style.display = "block";
        },
        success:  function (response) { //get request
          
        if(response.success){ 

        	window.location.href = "views/dashboard.php?suc=true"; 
	       
        }else{
          $("#successModalTitle").html("<i class='fas fa-exclamation-circle color-error'></i> ¡Error!");
          $("#successModalDescription").html(response.message);
          //alert(response.message);
          $('#alertModal').modal('toggle'); 
        }       
        }
      });

}

function confirm(){
  $('.close').click();
  document.getElementById("load").style.display = "none";
}